# bus

## folders & files
    +-- build/
    +-- config/
    +-- src
    |   +-- assets/
    	|   +-- (images)
    |   +-- components/
    	|   +-- (components)
    |   +-- router/
    	|   +-- index.js
    |   +-- styles/
    	|   +-- components/
    	|   +-- imports/
    	|   +-- layouts/
    	|   +-- partials/
    |   +-- index.vue
    |   +-- main.js
    +-- index.html
    +-- package.json
    +-- package-lock.json
    +-- README.md

## Demo
[https://chenej9797.gitlab.io/bus/](https://chenej9797.gitlab.io/bus/#/)


## Description
使用交通部公共運輸整合資訊平台提供的api，自行規劃視覺和介面並製作的「等公車」web app。

目前提供查詢公車路線、到站時間預估的功能，未來會陸續加上公車資訊、我的最愛等...功能。


## using...

- Vue
- Sass
- bourbon
- bourbon-neat
- material icons
- google fonts
- 公共運輸平台api


## 功能 beta.

- 首頁搜尋
- result page搜尋
- 顯示公車路線即時到站預估資訊
- 顯示到站公車車牌
- 顯示公車路線起迄點
- 去回程toggle


## TODO

- [x] switch origin & destination
- [x] mapping stops name & estimate info
- [x] refresh data every 10 second
- [ ] add bus info lightbox
- [x] adjust main page
- [x] loading animation
- [x] empty state
- [ ] add footer
- [x] 申請公共運輸平台會員 & 了解使用api的規則