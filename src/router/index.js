import Vue from 'vue'
import Router from 'vue-router'
import Main from '@/components/Main'
import Search from '@/components/Search'
// import VueAxios from 'vue-axios'
import axios from 'axios'

// axios不能通過Vue.use(axios)的方式import, 如果想在所有component中直接使用axios, 以下:
Vue.prototype.$http = axios;
Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'main',
      component: Main,
    },
    {
      path: '/search',
      name: 'search',
      component: Search,
    },
  ]
})
